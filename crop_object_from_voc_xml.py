import os
import glob
import time
import random
from typing import Awaitable
from lxml import etree

import cv2

XML_FOLDER_PATH = "Annotations"
IMG_FOLDER_PATH = "JPEGImages/img/jpg"
SAVE_FOLDER_PATH = "TableCropped"
TARGET_CLASSNAMES = ["Bordered", "Borderless"]


def crop_obj_from_image(xmlfilelist: list):
    broken_xmlfilelist = []
    image_not_exist_xml = []
    for xmlfile in xmlfilelist:
        print("---------")
        print("xmlfile: ", os.path.basename(xmlfile))
        try:
            # xmlの読み込み
            with open(xmlfile) as f:
                xml_string = f.read().encode('utf-8')
            dom = etree.fromstring(xml_string, parser=etree.XMLParser(recover=True))
        except:
            pass
        objs = dom.findall('object')
        print("len objs: ", len(objs))
        size = dom.find('size')
        width = int(size.find('width').text)
        height = int(size.find('height').text)

        counter = 0
        for obj in objs:
            classname = obj.find("name").text
            if classname not in TARGET_CLASSNAMES:
                continue
            bndbox = obj.find('bndbox')
            xmin = int(bndbox.find('xmin').text)
            ymin = int(bndbox.find('ymin').text)
            xmax = int(bndbox.find('xmax').text)
            ymax = int(bndbox.find('ymax').text)
            # yminとymaxの値が同じだったりするので(クローリングの失敗)
            if xmax - xmin <= 0 or ymax - ymin <= 0:
                broken_xmlfilelist.append(xmlfile)
                break
            # ymaxの値が画面のheightより大きかったりするので(クローリングの失敗)
            if ymax > height or xmax > width:
                broken_xmlfilelist.append(xmlfile)
                break

            print('xmin: ', xmin, 'xmax: ', xmax, 'ymin: ', ymin, 'ymax: ', ymax)
            # crop table from image
            imgfilename = os.path.basename(xmlfile).replace('.xml', '.jpg')
            img = cv2.imread(os.path.join(IMG_FOLDER_PATH, imgfilename))
            if not img is None:
                img_trim = img[ymin:ymax, xmin:xmax]
                cropped_filename = os.path.basename(xmlfile).replace('.xml', '') + '-' + str(counter).zfill(5) + '-' + classname + '.jpg'
                cv2.imwrite(os.path.join(SAVE_FOLDER_PATH, cropped_filename), img_trim) 
                counter += 1
            else:
                image_not_exist_xml.append(xmlfile)
                break 
    broken_str = '\n'.join(broken_xmlfilelist)
    with open("broken_xmlfile_list.txt", 'wt') as f:
        f.write(broken_str)
    not_exist_str = '\n'.join(image_not_exist_xml)
    with open("image_not_exist_xmlfile_list.txt", 'wt') as f:
        f.write(not_exist_str)



def main():
    xmlfilelist = sorted([os.path.join(os.getcwd(), path) for path in glob.glob(os.path.join(XML_FOLDER_PATH, "*.xml"))])
    crop_obj_from_image(xmlfilelist)


if __name__ == "__main__":
    main()